﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModelLayer.Interfaces;
using DataModelLayer.DataModels;
using AutoMapper;

namespace EntityDataLayer.Repositories
{
    public class UserRepository : BaseRepository<UserDataModel,User>
    {
        public UserRepository()
        {
            this.Configure();
        }

        public override void Add(UserDataModel _entity)
        {
            using (var context = new AdressBookDBEntities())
            {
                context.User.Add(GetMap(_entity));
                context.SaveChanges();
            }
        }

        public void Add(AdressBookDBEntities _context, UserDataModel _entity)
        {
            _context.User.Add(GetMap(_entity));

        }      
        
        public UserDataModel GetByID(int _id)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _entity = context.User.First(arg => arg.UserID == _id);
                return GetMap(_entity);
            }
        }

        public IEnumerable<UserDataModel> GetAll()
        {
            using (var context = new AdressBookDBEntities())
            {
                List<UserDataModel> list = new List<UserDataModel>();

                foreach (var _entity in context.User)
                {
                    list.Add(GetMap(_entity));
                }
                return list;
            }
        }

        public void DeleteById(int _id)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _entity = context.User.First(arg => arg.UserID == _id);
                context.User.Remove(_entity);
                context.SaveChanges();
            }
        }

        public void UpdateById(int _id, UserDataModel _entity)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _findentity = context.User.First(arg => arg.UserID == _id);
                _findentity = GetMap(_entity);
                context.Entry(_findentity).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public UserDataModel GetByNickname(AdressBookDBEntities _context, string _nickname)
        {
            var _entity=_context.User.FirstOrDefault(arg => arg.Nickname == _nickname);
            return GetMap(_entity);
        }
    }
}
