﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.Interfaces;
using DataModelLayer.DataModels;

namespace EntityDataLayer.Repositories
{
    public class PersonRepository : ICommonRepository<PersonDataModel>
    {

        public void Add(PersonDataModel _entity)
        {
            using (var context = new AdressBookDBEntities())
            {
                context.Person.Add(new Person()
                {
                    Adress = _entity.Adress,
                    Email = _entity.Email,
                    FirstName = _entity.Email,
                    LastName = _entity.LastName,
                    Phone = _entity.Phone,
                    UserID = _entity.UserID
                });
                context.SaveChanges();
            }
        }
        public void Add(AdressBookDBEntities _context, PersonDataModel _entity)
        {
            _context.Person.Add(new Person()
            {
                Adress = _entity.Adress,
                Email = _entity.Email,
                FirstName = _entity.Email,
                LastName = _entity.LastName,
                Phone = _entity.Phone,
                UserID = _entity.UserID
            });

        }

        public PersonDataModel GetByID(int _id)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _entity = context.Person.First(arg => arg.ID == _id);
                return new PersonDataModel()
                {
                    ID = _entity.ID,
                    Adress = _entity.Adress,
                    Email = _entity.Email,
                    FirstName = _entity.Email,
                    LastName = _entity.LastName,
                    Phone = _entity.Phone,
                    UserID = (int)_entity.UserID
                };
            }
        }

        public IEnumerable<PersonDataModel> GetAll()
        {
            using (var context = new AdressBookDBEntities())
            {
                List<PersonDataModel> list = new List<PersonDataModel>();

                foreach (var _entity in context.Person)
                {
                    list.Add(new PersonDataModel()
                    {
                        ID = _entity.ID,
                        Adress = _entity.Adress,
                        Email = _entity.Email,
                        FirstName = _entity.Email,
                        LastName = _entity.LastName,
                        Phone = _entity.Phone,
                        UserID = (int)_entity.UserID
                    });
                }
                return list;

            }
        }

        public void DeleteById(int _id)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _entity = context.Person.First(arg => arg.ID == _id);
                context.Person.Remove(_entity);
                context.SaveChanges();
            }
        }

        public void UpdateById(int _id, PersonDataModel _entity)
        {
            using (var context = new AdressBookDBEntities())
            {
                var _findentity = context.Person.First(arg => arg.ID == _id);
                _findentity.FirstName = _entity.FirstName;
                _findentity.LastName = _entity.LastName;
                _findentity.Phone = _entity.Phone;
                _findentity.Adress = _entity.Adress;
                _findentity.Email = _entity.Email;
                _findentity.UserID = _entity.UserID;
                context.Entry(_findentity).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
