﻿using AutoMapper;
using DataModelLayer.DataModels;
using DataModelLayer.Interfaces;
using System.Collections.Generic;

namespace EntityDataLayer.Repositories
{
    public abstract class BaseRepository<T1, T2> : Profile, ICommonRepository<T1>
    {
        protected override void Configure()
        {
            Mapper.CreateMap<User, UserDataModel>();
            Mapper.CreateMap<UserDataModel, User>();
            Mapper.CreateMap<Person, PersonDataModel>();
            Mapper.CreateMap<PersonDataModel, Person>();
        }
        protected T1 GetMap(T2 TSource)
        {
            return Mapper.Map<T2, T1>(TSource);
        }
        protected T2 GetMap(T1 TSource)
        {
            return Mapper.Map<T1, T2>(TSource);
        }

        public abstract void Add(T1 _entity);
        public abstract T1 GetByID(int _id);
        public abstract IEnumerable<T1> GetAll();
        public abstract void DeleteById(int _id);
        public abstract void UpdateById(int _id, T1 _entity);
    }
}