﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.DataModels;
using System.Data.SqlClient;

namespace SqlQueryDataLayer.Extentions
{
    public static class UserExt
    {
        public static UserDataModel GetUser(this SqlDataReader _dr)
        {
            return new UserDataModel
            {
                UserID = (int)_dr["UserID"],
                Nickname = (string)_dr["Nickname"],
                Password = (string)_dr["Password"]
            };
        }
    }
}
