﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.DataModels;
using System.Data.SqlClient;

namespace SqlQueryDataLayer.Extentions
{
    public static class PhoneExt
    {
        public static PersonDataModel GetPhone(this SqlDataReader _dr)
        {
            return new PersonDataModel
            {
                ID = (int)_dr["ID"],
                FirstName = (string)_dr["FirstName"],
                LastName = (string)_dr["LastName"],
                Phone = (string)_dr["Phone"],
                Email = (string)_dr["Email"],
                Adress = (string)_dr["Adress"],
                UserID = (int)_dr["UserID"]
            };
        }
    }
}
