﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.Interfaces;
using DataModelLayer.DataModels;
using System.Data.SqlClient;
using SqlQueryDataLayer.Extentions;

namespace SqlQueryDataLayer.Repositories
{
    public class PersonRepository : BaseRepository, ICommonRepository<PersonDataModel>
    {
        public PersonRepository()
        {
        }
        #region InterfaceImplementation
        public void Add(PersonDataModel _entity)
        {
            string sqlGueryString = String.Format(@"insert into Person(FirstName,LastName,Phone,Email,Adress,UserID)
values ('{0}','{1}','{2}','{3}','{4}','{5}');",
                _entity.FirstName, _entity.LastName, _entity.Phone,_entity.Email,_entity.Adress,_entity.UserID);
            ExecuteSQLString(sqlGueryString);
        }
        public IEnumerable<PersonDataModel> GetAll()
        {
            var _personList = new List<PersonDataModel>();
            using (var cn = GetConnection())
            {
                //cn.ConnectionString = connect.ConnectionString; 
                try
                { //Открыть подключение

                    cn.Open();
                    string sqlGueryString = @"Select * from Person";
                    SqlCommand myCommand = new SqlCommand(sqlGueryString, cn);
                    SqlDataReader dr = myCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        //расширяющий метод GetProduct для dr чтобы он возвращал один продукт
                        _personList.Add(dr.GetPhone());
                    }

                }
                catch (SqlException _ex)
                { // Протоколировать исключение
                    throw _ex;
                }
                finally
                { // Гарантировать освобождение подключения
                    cn.Close();
                }

            }
            return _personList;
        }

        

        public void DeleteById(int _id)
        {
            string sqlGueryString = String.Format(@"DELETE FROM Person WHERE ID={0};", _id);
            ExecuteSQLString(sqlGueryString);
        }

        public void UpdateById(int _id, PersonDataModel _entity)
        {            
            string sqlGueryString = String.Format(@"update Person set FirstName='{0}', LastName='{1}', 
Phone='{2}',Email='{3}',Adress='{4}' where ID={5};",
                _entity.FirstName,_entity.LastName,_entity.Phone,_entity.Email,_entity.Adress,_id);
            ExecuteSQLString(sqlGueryString);
        }
        
        public PersonDataModel GetByID(int _id)
        {
            throw new NotImplementedException();
        }
        #endregion
        public IEnumerable<PersonDataModel> GetByUserID(int _userID)
        {
            var _personList = new List<PersonDataModel>();
            using (var cn = GetConnection())
            {
                //cn.ConnectionString = connect.ConnectionString; 
                try
                { //Открыть подключение

                    cn.Open();
                    string sqlGueryString =String.Format(@"Select * from Person WHERE UserID={0};",_userID);
                    SqlCommand myCommand = new SqlCommand(sqlGueryString, cn);
                    SqlDataReader dr = myCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        //расширяющий метод GetProduct для dr чтобы он возвращал один продукт
                        _personList.Add(dr.GetPhone());
                    }

                }
                catch (SqlException _ex)
                { // Протоколировать исключение
                    throw _ex;
                }
                finally
                { // Гарантировать освобождение подключения
                    cn.Close();
                }

            }
            return _personList;
        }
    }
}
