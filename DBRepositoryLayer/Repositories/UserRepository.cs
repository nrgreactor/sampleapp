﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.Interfaces;
using DataModelLayer.DataModels;
using System.Data.SqlClient;
using SqlQueryDataLayer.Extentions;

namespace SqlQueryDataLayer.Repositories
{
    public class UserRepository:BaseRepository,ICommonRepository<UserDataModel>
    {

        public void Add(UserDataModel _entity)
        {
            string sqlGueryString = String.Format(@"insert into User(Nickname,Password) values ('{0}','{1}');",
                _entity.Nickname,_entity.Password);
            ExecuteSQLString(sqlGueryString);
        }

        public UserDataModel GetByID(int _id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserDataModel> GetAll()
        {
            var _userList = new List<UserDataModel>();
            using (var cn = GetConnection())
            {
                //cn.ConnectionString = connect.ConnectionString; 
                try
                { //Открыть подключение

                    cn.Open();
                    string sqlGueryString = @"Select * from User";
                    SqlCommand myCommand = new SqlCommand(sqlGueryString, cn);
                    SqlDataReader dr = myCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        //расширяющий метод GetUser для dr чтобы он возвращал один продукт
                        _userList.Add(dr.GetUser());
                    }

                }
                catch (SqlException _ex)
                { // Протоколировать исключение
                    throw _ex;
                }
                finally
                { // Гарантировать освобождение подключения
                    cn.Close();
                }

            }
            return _userList;
        }

        public void DeleteById(int _id)
        {
            string sqlGueryString = String.Format(@"DELETE FROM User WHERE UserID={0};", _id);
            ExecuteSQLString(sqlGueryString);
        }

        public void UpdateById(int _id, UserDataModel _entity)
        {
            string sqlGueryString = String.Format(@"update User set Nickname='{0}', Password='{1}' where UserID={2};",
               _entity.Nickname,_entity.Password,_id);
            ExecuteSQLString(sqlGueryString);
        }
        
    }
}
