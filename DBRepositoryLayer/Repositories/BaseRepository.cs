﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SqlQueryDataLayer.Repositories
{
    public abstract class BaseRepository
    {
        private string _entityNameConnection = "PhoneBookEntities";
        private string _sqlNameConnection = "PhoneBookSQL";


        private string GetEntityConnectionString()
        {
            if (string.IsNullOrEmpty(_entityNameConnection)) throw new Exception("Name connection is not define");
            var _connectionString = ConfigurationManager.ConnectionStrings[_entityNameConnection].ConnectionString;
            if (string.IsNullOrEmpty(_connectionString)) throw new Exception("Connection String is not define");
            return _connectionString;
        }

        private string GetSQLConnectionString()
        {
            if (string.IsNullOrEmpty(_sqlNameConnection)) throw new Exception("Name connection is not define");
            var _connectionString = ConfigurationManager.ConnectionStrings[_sqlNameConnection].ConnectionString;
            if (string.IsNullOrEmpty(_connectionString)) throw new Exception("Connection String is not define");
            return _connectionString;
            /*SqlConnectionStringBuilder builder =  new SqlConnectionStringBuilder();
            builder["Data Source"] = "7419_06";
            builder["integrated Security"] = true;
            builder["Initial Catalog"] = "PhoneBook";
            return builder.ConnectionString;*/
        }

        protected SqlConnection GetConnection()
        {
            return new SqlConnection(GetSQLConnectionString());
        }

        protected void ExecuteSQLString(string _sqlString)
        {
            using (var cn = GetConnection())
            {
                //cn.ConnectionString = connect.ConnectionString; 
                try
                { //Открыть подключение
                    cn.Open();
                    SqlCommand myCommand = new SqlCommand(_sqlString, cn);
                    myCommand.ExecuteNonQuery();
                }
                catch (SqlException _ex)
                { // Протоколировать исключение
                    throw _ex;
                }
                finally
                { // Гарантировать освобождение подключения
                    cn.Close();
                }
            }
        }
    }
}
