﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModelLayer.Interfaces;
using DataModelLayer.DataModels;
using System.IO;
using System.Xml.Serialization;
namespace XMLDataLayer.Repositories
{
    public class PersonRepository : ICommonRepository<PersonDataModel>
    {
        public PersonRepository(string @_filePath)
        {
            this._filePath = _filePath;
            ReadXML();
        }
        public PersonRepository()
        {
            this._filePath = "Person_List.XML";
            ReadXML();
        }
        private string _filePath;
        private List<PersonDataModel> _personList = new List<PersonDataModel>();        
        private void SaveChanges()
        {
            using (var fs = new FileStream(@_filePath, FileMode.Create))
            {
                // Создаем объект XmlSerializer для выполнения // сериализации
                XmlSerializer xs = new XmlSerializer(typeof(List<PersonDataModel>));
                // Используем объект XmlSerializer для //  сериализации данных в файл 
                xs.Serialize(fs, _personList);
                fs.Close();
            }           
        }
        private void ReadXML()
        {
            if (File.Exists(_filePath))
            using (var fs = new FileStream(@_filePath, FileMode.Open))
            {
                // Создаем объект XmlSerializer для выполнения // сериализации
                XmlSerializer xs = new XmlSerializer(typeof(List<PersonDataModel>));
                // Используем объект XmlSerializer для //  сериализации данных в файл
                _personList=(List<PersonDataModel>) xs.Deserialize(fs);
            }    
        }
        private int AutoGenerateID()
        {
            if (_personList.Count > 0)
                return _personList.Max(x => x.ID) + 1;
            else return 1;
        }
        
        #region InterfaceImplementation
        public void Add(PersonDataModel _entity)
        {
            _entity.ID = AutoGenerateID();
            _personList.Add(_entity);
            SaveChanges();
        }

        public PersonDataModel GetByID(int _id)
        {
            try
            {
                return _personList.First(o => o.ID == _id);
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }

        public IEnumerable<PersonDataModel> GetAll()
        {
            return _personList;
        }

        public void DeleteById(int _id)
        {
            try
            {
                _personList.RemoveAll(o => o.ID == _id);
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            SaveChanges();
        }

        public void UpdateById(int _id, PersonDataModel _entity)
        {
            try
            {
                _personList.Where(o => o.ID == _id).Select(s => { s = _entity; return s; }).ToList();
                SaveChanges();
            }
            catch (Exception _ex)
            {
                throw _ex;
            }

        }
        #endregion
        
    }
}
