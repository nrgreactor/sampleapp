﻿using DataModelLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SampleConsoleApp
{
    class Manager
    {
        private XMLDataLayer.Repositories.PersonRepository _xmlRepo;
        private SqlQueryDataLayer.Repositories.PersonRepository _dbRepo;
        public Manager()
        {
            _xmlRepo = new XMLDataLayer.Repositories.PersonRepository("PersonList");
            _dbRepo = new SqlQueryDataLayer.Repositories.PersonRepository();
        }
        public void ShowAll()
        {
            Console.WriteLine("Select from xml:");
            foreach (var item in _xmlRepo.GetAll())
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine("Select from SqlQuery:");
            foreach (var item in _dbRepo.GetAll())
            {
                Console.WriteLine(item.ToString());
            }
        }

        public void AddToALLDB()
        {
            PersonDataModel _item = new PersonDataModel();
            Console.Write("Enter First Name: ");
            _item.FirstName = Console.ReadLine();
            Console.Write("Enter Last Name: ");
            _item.LastName = Console.ReadLine();
            Console.Write("Enter Phone: ");
            _item.Phone = Console.ReadLine();
            Console.Write("Enter Email: ");
            _item.Email = Console.ReadLine();
            Console.Write("Enter Adress: ");
            _item.Adress = Console.ReadLine();
            Console.Write("Enter UserID: ");
            _item.UserID =Convert.ToInt32(Console.ReadLine());
            _xmlRepo.Add(_item);
            _dbRepo.Add(_item);
            Console.WriteLine("Done.");
        }
    }
}
