﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModelLayer.Interfaces
{
    public interface ICommonRepository<T>
    {
         void Add(T _entity);
         T GetByID(int _id);
         IEnumerable<T> GetAll();
         void DeleteById(int _id);
         void UpdateById(int _id, T _entity);
    }
}
