﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModelLayer.DataModels
{
    [Serializable]
    public class PersonDataModel
    {
        #region Fields
        private int _id;
        private string _firstName;
        private string _lastName;
        private string _phone;
        private string _email;
        private string _adress;
        private int _userid;
        #endregion
        #region Properties

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                this._id = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                this._firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                this._lastName = value;
            }
        }
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                this._phone = value;
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                this._email = value;
            }
        }
        public string Adress
        {
            get
            {
                return _adress;
            }
            set
            {
                this._adress = value;
            }
        }
        public int UserID
        {
            get
            {
                return _userid;
            }
            set
            {
                this._userid = value;
            }
        }

        #endregion
        public override string ToString()
        {
            return String.Format("#{0}\n{1}\n{2}\n{3}\n{4}\n{5}", _id, _firstName, _lastName,_phone,_email,_adress);
        }
    }
}
