﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModelLayer.DataModels
{
    [Serializable]
    public class UserDataModel
    {
        #region Fields
        private int _userid;
        private string _nickname;
        private string _password;
        #endregion
        #region Properties
        public int UserID
        {
            get { return _userid; }
            set { this._userid = value; }
        }

        public string Nickname
        {
            get { return _nickname; }
            set { this._nickname = value; }
        }

        public string Password
        {
            get { return _password; }
            set { this._password = value; }
        }
        #endregion
    }
}
